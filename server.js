const express = require('express');
const url = require('url');
const proxy = require('express-http-proxy');




const app = express();
const port = process.env.PORT || 5000;

app.get('/api/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});

app.use('/api', proxy({target: 'https://od-api.oxforddictionaries.com/api', changeOrigin: true}));

app.listen(port, () => console.log(`Listening on port ${port}`));