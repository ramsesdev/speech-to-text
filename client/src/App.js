import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SpeechForm from './components/SpeechForm';
import Request from './API/OxfordDictionary';

class App extends Component {
  componentWillMount (){
    Request();
  }
  render() {
    return (
      <div className="container App">
        <div className="row">
          <div className="col s12">
            <SpeechForm />
            
          </div>
          <div className="col s6">
          </div>
          <div className="col s6"></div>
        </div>
      </div>
    );
  }
}

export default App;
