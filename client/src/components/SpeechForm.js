import React from "react";
import words from "../API/words.json"
import {Button, Icon} from 'react-materialize'

class SpeechForm extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      currentWord: null,
      gameIsRunning: false,
      userInput: "",
      userScore: 0,
      scoreCounter: 0,
      userLevel: 1,
      progressBar: 0,
      
    }
    this.handleSpeech = this.handleSpeech.bind(this);
    this.initGame = this.initGame.bind(this);
    this.updateUserInput = this.updateUserInput.bind(this)
    this.handlePlayAgain = this.handlePlayAgain.bind(this)
    this.updateScore = this.updateScore.bind(this)
    this.updateUserLevel = this.updateUserLevel.bind(this)
    this.updateProgressBar = this.updateProgressBar.bind(this)
  }
  
  // handleTextChange = e => {
  //   this.setState({ name: e.target.value });
  // };
  
  // handleTextChange = function(e) {
  //   this.setState({ name: e.target.value });
  // }.bind(this);
  //
  random(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }
  getNewWord(){
    var wordsWithNLetters = this.state.userLevel;
    var wordsSection = words["w-"+wordsWithNLetters];
    console.log(words[0], words.length)
    console.log("wordsSection: ",wordsSection,"w-"+wordsWithNLetters);
    
    var totalWords = wordsSection.length  && wordsSection.length-1;
    console.log( "totalWords: ",totalWords);
    
    var randomWord = this.random(0,totalWords);
    console.log("randomWordPosition: ", randomWord)
    var word = wordsSection[randomWord]
    console.log("selecredWord: ", word)
    
    return word;
  }
  updateUserLevel(){
    console.log(this.state.userScore );
    if(this.state.userScore > 0 && this.state.userScore % 10 === 0){
      this.setState({
        userLevel: ++this.state.userLevel,
        scoreCounter: 0
      })
    }
    
  }
  speakWord(wordParam = null){
    let wordToSpeak = wordParam || this.getNewWord();
    
    if(!wordParam){
      this.setState({currentWord: wordToSpeak})
      this.setState({userInput: ''});
    }
    window.responsiveVoice.speak(wordToSpeak);
  }
  handlePlayAgain(){
    this.speakWord(this.state.currentWord)
  }
  updateUserInput(e){
    this.setState({userInput: e.target.value});
  }
  
  handleSpeech(e) {
    e.preventDefault();
    let userInput = this.state.userInput.toLowerCase();
    let currentWord = this.state.currentWord.toLowerCase();
    
    if(!this.state.gameIsRunning) return;
    if(userInput !== currentWord){
      this.setState({ message: "Vocë errou, tente novamente!"});
    }else{
      this.setState({message: "Parabéns, vamos para a próxima!"});
      this.updateScore();
      this.updateProgressBar();
      this.updateUserLevel();
      
      this.speakWord();
      
    }
    
  }
  updateScore(score){
    this.setState({
      userScore: ++this.state.userScore,
      scoreCounter: ++this.state.scoreCounter
    });
    console.log("Score: ", this.state.userScore)
    
  }
  updateProgressBar(){
    let value = (this.state.scoreCounter*100)/10;
    console.log("=====",this.state.userScore ,"((",this.state.userScore*100,"))",(this.state.userScore*100)/10)
    if(value >=100){
      value = 0;
    }
    this.setState({progressBar : value})
    
  }
  initGame(){
    this.setState({"gameIsRunning": true})
    this.speakWord();
  }
  
  render() {
    return (
      <div>
        <header className="App-header">
          <h1 className="App-title">Speech to Text</h1>
          <div>Your score: {this.state.userScore} </div>
          <div>Level: {this.state.userLevel}</div>
        </header>
        <p>
        userInput = {this.state.userInput}<br />
        currentWord = {this.state.currentWord}<br />
        </p>
        
        <form onSubmit={this.handleSpeech}>

          {this.state.gameIsRunning ? (
            <div>
              <div className="progress">
                <div className="determinate" style={{width: this.state.progressBar+'%'}}></div>
              </div>
              <input
                    type="text"
                    value={this.state.userInput}
                    onChange={this.updateUserInput}/>
            <Button waves='light' type="button" onClick={this.handlePlayAgain}>Replay</Button>
            <Button waves='light' type="submit" className="btn waves-effect waves-light">Submit</Button>
            </div>
          ) : <Button waves='light' type="button" onClick={this.initGame}>Start</Button>}
        </form>
        <strong>{this.state.message}</strong>
      </div>
      
    );
  }
}

export default SpeechForm;
